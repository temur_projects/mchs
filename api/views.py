import json
from django.core.cache import cache
from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.info.models import DbVersion, Info
from apps.message.models import Message, File, Address
from apps.message.utils import file_seperate
from apps.user_profile.models import Profile
from utils.sms import send_sms
from utils.string import random_str
from .serializers import InfoSerializer
from geopy.geocoders import Nominatim
from utils.playmobile import send_sms as sms


class MessageCreateAPIView(APIView):
    parser_classes = (FormParser, MultiPartParser)

    authentication_classes = [TokenAuthentication, ]

    def post(self, request):
        data = dict(json.loads(request.POST.get('data')))
        message = Message()
        message.text = data.get('about')
        message.user = self.request.user
        message.altitude = data.get("altitude")
        message.longitude = data.get("longitude")
        message.title = data.get('type')
        message.save()
        geolocator = Nominatim(user_agent="mchs")
        location = geolocator.reverse(str(message.altitude) + ', ' + str(message.longitude), language='ru')

        address = Address.objects.filter(state=location.raw.get('address').get('city'),
                                         district__icontains=location.raw.get('address').get('county')[:5])
        if address.exists():
            address = address[0]
            address.quantity += 1
            address.save()

        images, videos = file_seperate(request.data.getlist("file"))

        if len(images) > 0:
            for img in images:
                File.objects.create(message=message, file=img)
        if len(videos) > 0:
            for vid in videos:
                File.objects.create(message=message, file=vid)

        # for data in request.data.getlist("file"):
        #     File.objects.create(message=message, file=data)

        return Response(data={'status': 'ok'})


class LoginView(APIView):

    def post(self, request):
        user = User.objects.get(username=request.data['username'])
        if user.check_password(request.data['password']):
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
        else:
            return Response(status=401)


class UserRegister(APIView):

    def post(self, request):
        u = User.objects.filter(username=request.data.get('username'))
        if not u.exists():
            user = User()
            user.username = request.data.get('username')
            user.set_password(request.data.get('password'))
            user.is_active = False
            user.save()
            return Response(status=201)
        else:
            code = random_str()
            u[0].profile.code = code
            u[0].profile.save()
            send_sms(u[0].username, 'mchs', code)
            return Response(status=201)


class ConfirmView(APIView):
    def get(self, request):
        username = request.GET.get('username')
        code = request.GET.get('confirm_code')
        try:
            user = User.objects.get(username=username)
            if user.profile.code == code:
                token, created = Token.objects.get_or_create(user=user)
                user.is_active = True
                user.save()
                return Response({'token': token.key}, status=202)
            else:
                return Response(status=406)
        except User.DoesNotExist:
            return Response(status=406)


class SyncWikiView(APIView):
    # authentication_classes = [TokenAuthentication, ]

    def get(self, request):
        db_version = request.GET.get('dbVersion')
        my_db_version = DbVersion.objects.get(pk=1).version
        if my_db_version == db_version:
            return Response(status=304)
        else:
            data = InfoSerializer(Info.objects.all(), many=True, context={'request': request})
            # data = Info.objects.all().values("content", "last_modified", "image")
            return Response(data=data.data)


class PasswordRecover(APIView):

    def post(self, request):
        print(request.META)
        return Response()

    def get(self, request):
        print(request.META)
        import urllib.parse
        phone = urllib.parse.unquote(request.GET.get("phone"))
        print(phone)
        code = request.GET.get("code")
        passwd = request.GET.get("password")
        if phone and not code and not passwd:
            try:
                user = User.objects.get(username=phone)
                gen_code = random_str()
                status = sms(user.username.replace("+", ""), gen_code)
                if status == 200:
                    cache.set(str(gen_code), user.username, 6000)
                    return Response(status=200)
                else:
                    return Response(status=502)
            except Profile.DoesNotExist:
                return Response(status=404)
        if phone and code:
            user = cache.get(code)
            if user == phone:
                return Response(status=200)
            return Response(status=404)
        if phone and passwd:
            user = User.objects.get(username=phone)
            user.set_password(passwd)
            user.save()
            return Response(status=200)
        return Response(status=200, data={'sssss': 1111})
