from django.urls import path

from .views import MessageCreateAPIView, LoginView, UserRegister, ConfirmView, SyncWikiView, \
    PasswordRecover


urlpatterns = [
    path('password-recover/', PasswordRecover.as_view()),
    path('alert/', MessageCreateAPIView.as_view()),
    path('login/', LoginView.as_view()),
    path('signup/', UserRegister.as_view()),
    path('confirm/', ConfirmView.as_view()),
    path('syncwiki/', SyncWikiView.as_view())
]
