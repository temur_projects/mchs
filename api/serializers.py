from rest_framework import serializers
from apps.info.models import Info


class InfoSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()

    class Meta:
        model = Info
        fields = [
            'id',
            'title',
            'content',
            'image'
        ]

    def get_image(self, info):
        request = self.context.get('request')
        photo_url = info.image.url
        return request.build_absolute_uri(photo_url)
