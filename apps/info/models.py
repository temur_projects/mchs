from django.db import models


def file_handler(instance, filename):
    return f'info/{filename}'


class DbVersion(models.Model):
    version = models.IntegerField(default=1)


class Info(models.Model):
    title = models.CharField(max_length=255, blank=True, default='')
    content = models.TextField()
    last_modified = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to=file_handler, blank=True)

    class Meta:
        ordering = ('-id',)
