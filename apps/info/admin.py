from django.contrib import admin
from apps.message.models import Address
# Register your models here.
from .models import Info
from .forms import InfoForm

admin.site.register(Address)
class YourModelAdmin(admin.ModelAdmin):
    form = InfoForm


admin.site.register(Info, YourModelAdmin)
