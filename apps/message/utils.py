from django.conf import settings


def file_seperate(files) -> tuple:
    print(files)
    print(type(files))
    images = []
    videos = []
    for file in files:
        ext = file.name.split('.')[-1]
        if ext in settings.ALLOWED_IMAGE_TYPES: images.append(file)
        if ext in settings.ALLOWED_VIDEO_TYPES: videos.append(file)
    return images, videos
