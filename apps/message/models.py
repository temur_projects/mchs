from django.conf import settings
from django.db import models
from django.contrib.auth.models import User


def file_handler(instance, filename):
    return f'{instance.message.user.username}/{filename}'


class Message(models.Model):
    user = models.ForeignKey(User, related_name='messages', on_delete=models.CASCADE)
    title = models.CharField(max_length=40, blank=True, default='')
    altitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    text = models.TextField(blank=True, default='')
    created = models.DateTimeField(auto_now_add=True)
    is_read = models.BooleanField(default=False)
    is_solved = models.BooleanField(default=False)

    class Meta:
        ordering = ['-created', ]

    def __str__(self):
        if self.title:
            return self.title
        return 'Сообщение'

    def __check_image_extention(self, file)->bool:
        if file.file.name.split(".")[-1] in settings.ALLOWED_IMAGE_TYPES:
            return True

    def __check_video_extention(self, file)->bool:
        if file.file.name.split(".")[-1] in settings.ALLOWED_VIDEO_TYPES:
            return True

    def get_videos(self):
        return [file for file in self.files.all() if self.__check_video_extention(file)]

    def get_images(self):
        return [file for file in self.files.all() if self.__check_image_extention(file)]


class FileManager(models.Manager):
    def only_videos(self):
        return self.filter()

    def only_images(self):
        pass


class File(models.Model):
    message = models.ForeignKey(Message, related_name='files', blank=True, on_delete=models.CASCADE)
    file = models.FileField(upload_to=file_handler, blank=True)
    objects = FileManager


class Address(models.Model):
    state = models.CharField(max_length=100, default="")
    district = models.CharField(max_length=100, default="")
    quantity = models.IntegerField(default=0)

    def __str__(self):
        return self.district
