from django import template

from apps.message.models import Message

register = template.Library()


@register.simple_tag(name='unread_message_counter')
def unread_message_counter():
    return Message.objects.filter(is_read=False).count()


@register.filter
def in_category(things):
    return things.messages.filter(is_read=True).count()


@register.filter
def in_category1(things):
    return things.messages.filter(is_solved=True).count()
# @register.inclusion_tag('base.html')
# def message_counter():
#     return {'messages1':Message.objects.filter(is_read=False)[:3]}
