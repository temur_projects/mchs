from django.urls import path, include

from .views import IndexView, UserListView, UserMessageListView, MessageDetailView, MessageListView, \
    logout_view, login_view, help_list_add, HelpList, help_list_edit, HelpView, Tashkent, date_stats, \
    HelpListDelete

helplist_patterns = [
    path('', HelpList.as_view(), name='HelpList'),
    path('delete/<int:helplist_id>/', HelpListDelete.as_view(), name='helplist_delete'),
    path('add/', help_list_add, name='help_list_add'),
    path('<int:pk>/', HelpView.as_view(), name='help_view'),
    path('<int:id>/edit/', help_list_edit, name='help_list_edit'),
]

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('helplist/', include(helplist_patterns)),
    path('tashkent/', Tashkent.as_view(), name="Tashkent"),
    path('datestats/', date_stats, name='date_stats'),
    path('logout/', logout_view, name='logout'),
    path('login/', login_view, name='login'),
    path('message/list/', MessageListView.as_view(), name='message_list'),
    path('user/list/', UserListView.as_view(), name='user_list'),
    path('user/<int:user_id>/messages/', UserMessageListView.as_view(), name='user_messages'),
    path('user/<int:user_id>/messages/<int:id>/', MessageDetailView.as_view(), name='message_detail'),
]
