from django import forms

years = [t for t in range(2018, 2030)]
months = [t for t in range(1, 13)]


class DateForm(forms.Form):
    from_date = forms.DateTimeField(widget=forms.SelectDateWidget(), label="С")
    to_date = forms.DateTimeField(widget=forms.SelectDateWidget(), label="До")
