from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView, ListView, DetailView
from apps.info.models import Info, DbVersion
from apps.info.forms import InfoForm
from apps.message.models import Message, Address
from .forms import DateForm
import calendar


class IndexView(TemplateView):
    template_name = 'base.html'


class UserListView(ListView):
    template_name = 'user_list.html'
    model = User
    # paginate_by = 20
    context_object_name = 'users'


class UserMessageListView(ListView):
    template_name = 'user_messages.html'
    model = Message
    # paginate_by = 20
    context_object_name = 'messages'

    def get_queryset(self):
        user = self.kwargs.get('user_id')
        return Message.objects.filter(
            user__id=int(user)
        )

    def get_context_data(self, **kwargs):
        resp = super().get_context_data(**kwargs)
        resp['user'] = User.objects.get(id=int(self.kwargs.get('user_id')))
        return resp


class MessageDetailView(DetailView):
    template_name = 'message_detail.html'
    model = Message
    context_object_name = 'message'
    pk_url_kwarg = 'id'

    def post(self, request, *args, **kwargs):
        is_solved = request.POST.get('is_solved')
        is_read = request.POST.get('is_read')
        msg = self.get_object()
        if is_read is not None:
            msg.is_read = True
        if is_solved is not None:
            msg.is_solved = True

        msg.save()
        return super().get(request, *args, **kwargs)


class MessageListView(ListView):
    model = Message
    template_name = 'message_list.html'
    context_object_name = 'messages'

    def get_context_data(self, **kwargs):
        cont = self.request.GET
        resp = super().get_context_data(**kwargs)
        if cont:
            # print(cont)
            resp['date_form'] = DateForm(cont)

        else:
            resp['date_form'] = DateForm()
        return resp

    # def post(self, request):
    #     qs = self.get_queryset(data=request.POST)

    def get_queryset(self, data=None):

        if self.request.method == 'GET':
            import datetime
            data = self.request.GET
            from_date_2 = datetime.date(
                int(data.get('from_date_year', 2010)),
                int(data.get('from_date_month', 1)),
                int(data.get('from_date_day', 1))
            )
            to_date_2 = datetime.date(
                int(data.get('to_date_year', 2050)),
                int(data.get('to_date_month', 1)),
                int(data.get('to_date_day', 1))
            )
            # from_date = self.request.GET.get('from_date_year') + '-' + self.request.GET.get(
            #     'from_date_month') + '-' + self.request.GET.get('from_date_day')
            # to_date = self.request.GET.get('to_date_year') + '-' + self.request.GET.get(
            #     'to_date_month') + '-' + self.request.GET.get('to_date_day')
            # if to_date >= from_date:
            #     return Message.objects.filter(created__range=[from_date, to_date])
            # if to_date_2 >= from_date_2:
            return Message.objects.filter(created__range=[from_date_2, to_date_2])
        return Message.objects.all()


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user and user.is_superuser:
            login(request, user)
            return HttpResponseRedirect('/')
        else:
            return render(request, 'login.html', {'error': 'error'})
    else:
        return render(request, 'login.html')


def help_list_add(request):
    if request.method == 'POST':
        info_form = InfoForm(request.POST, request.FILES)
        if info_form.is_valid():
            info_form.save()
            if not DbVersion.objects.all():
                DbVersion.objects.create(id=1)
            db_version = DbVersion.objects.get(id=1)
            db_version.version += 1
            db_version.save()
            return HttpResponseRedirect('/helplist/')

    else:
        info_form = InfoForm()
    return render(request, 'help_list_add.html', {'info_form': info_form})


class HelpListDelete(View):
    def get(self, request, helplist_id):
        try:
            info = Info.objects.get(id=int(helplist_id))
            info.delete()
        except Info.DoesNotExist:
            pass
        return redirect("/helplist")


class HelpList(ListView):
    model = Info
    paginate_by = 20
    template_name = 'help_list.html'
    context_object_name = 'infos'

    def post(self, request, *args, **kwargs):
        db_version = DbVersion.objects.get(pk=1)
        db_version.version += 1
        db_version.save()
        info = Info.objects.get(id=int(request.POST.get('DELETE')))
        info.delete()
        return super().get(request, *args, **kwargs)


class HelpView(DetailView):
    template_name = 'help_view.html'
    model = Info
    context_object_name = 'info'


def help_list_edit(request, id):
    info = Info.objects.get(id=id)
    if request.method == 'POST':
        info_form = InfoForm(instance=info,
                             data=request.POST,
                             files=request.FILES)
        print(info_form)
        if info_form.is_valid():
            info_form.save()
            db_version = DbVersion.objects.get(pk=1)
            db_version.version += 1
            db_version.save()
            return HttpResponseRedirect(reverse('help_list_edit', args=[info.pk, ]))
    else:
        info_form = InfoForm(instance=info)
        print(info_form)
    return render(request, 'help_list_edit.html', {'info_form': info_form})


class Tashkent(ListView):
    template_name = 'tashkent.html'
    model = Address
    context_object_name = 'addresses'

    def get_context_data(self, **kwargs):
        resp = super().get_context_data(**kwargs)
        resp["Sergili"] = Address.objects.get(district='Сергелийский район')
        resp["Mirzo"] = Address.objects.get(district='Мирзо-Улугбекский район')
        resp["Mirobod"] = Address.objects.get(district='Мирабадский район')
        resp["Bektemir"] = Address.objects.get(district='Бектемирский район')
        resp["Olmazor"] = Address.objects.get(district='Алмазарский район')
        resp["Yashnobod"] = Address.objects.get(district='Яшнободский район')
        resp["Yunusobod"] = Address.objects.get(district='Юнусабадский район')
        resp["Uchtepa"] = Address.objects.get(district='Учтепинский район')
        resp["Shayxontohur"] = Address.objects.get(district='Шайхантахурский район')
        resp["Chilonzor"] = Address.objects.get(district='Чиланзарский район')
        resp["Yakkasaroy"] = Address.objects.get(district='Яккасарайский район')
        return resp


def date_stats(request):
    d = {}
    for t in range(1, 13):
        d[calendar.month_name[t]] = Message.objects.filter(created__month=t).count()
    context = {
        'd': d
    }
    return render(request, 'date_stats.html', context)
