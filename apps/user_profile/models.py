from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save


from django.contrib.auth.models import User

from utils.sms import send_sms
from utils.string import random_str


class Profile(models.Model):
    user = models.OneToOneField('auth.User', related_name='profile', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True)
    last_name = models.CharField(max_length=100, blank=True)
    phone = models.CharField(max_length=100, blank=True)
    code = models.CharField(max_length=6, blank=True)

    def __str__(self):
        return self.user.username

    def fullname(self):
        return f'{self.first_name} {self.last_name}'


@receiver(post_save, sender=User)
def send_confirm_sms(instance, created, **kwargs):
    if created:
        code = random_str()
        send_sms(instance.username, 'mchs', code)
        Profile.objects.create(user=instance, code=code)
