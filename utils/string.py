import random
import string


def random_str(size=6, st=string.digits) -> str:
    return ''.join(random.choice(st) for _ in range(size))
