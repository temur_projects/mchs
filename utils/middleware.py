from django.shortcuts import redirect


class LoginRequired:
    """
    If request user is not authenticated
    redirect to auth page
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        urls = ['admin', 'login', 'api']
        if not request.user.is_authenticated:
            if request.path.split('/')[1] in urls:  # exclude the urls
                pass
            else:
                return redirect('/login/')
        return self.get_response(request)
