import requests


def send_sms(number: str, text: str) -> int:
    url = "https://ofs.uz/software.uz/core/sms_send?id=1255&message=%s&phone=%s"
    response = requests.get(url % (text, number))
    return response.status_code
