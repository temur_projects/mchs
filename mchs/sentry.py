import raven
import os
from django.conf import settings

settings.INSTALLED_APPS += [
    'raven.contrib.django.raven_compat',
]

settings.MIDDLEWARE += [
    'raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware'
]

RAVEN_CONFIG = {
    'dsn': 'https://7fd6dfc550024d0b96402f2ee7b6145a:8332649a7ac8488388c7b69b218e7ab4@sentry.io/1269462',
    'release': raven.fetch_git_sha(os.path.abspath(os.path.dirname(os.path.dirname(__file__)))),
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
            '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',  # To capture more than ERROR, change to WARNING, INFO, etc.
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}
