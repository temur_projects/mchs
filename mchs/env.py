import os

db_user = os.environ.get("MCHS_DB_USER")  # database username
db_passwd = os.environ.get("MCHS_DB_PASSWD")  # database username's password

os.environ.setdefault("MCHS_DEBUG", "True")
